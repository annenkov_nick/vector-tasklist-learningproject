let path = require('path'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    CleanWebpackPlugin = require('clean-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    MiniCssExtractPlugin = require("mini-css-extract-plugin");


module.exports = {

//----  Directories Configuration  -----------------------------------------------------------------------------------//

    entry: "./js/main.jsx",
    context: path.resolve(__dirname, 'app'),                       //Sets up entry root directory

    output: {
        filename: 'main.js',
        chunkFilename: "[name].bundle.js",
        path: path.resolve(__dirname, 'build')
    },

//----  Plugins Configuration  ---------------------------------------------------------------------------------------//

    plugins: [
        new CleanWebpackPlugin(['build']),
        new HtmlWebpackPlugin({
            template: 'index.html'
        }),
        new CopyWebpackPlugin([
            {from: './image', to: './image'},
            {from: './fonts', to: './fonts'}
        ]),
        new MiniCssExtractPlugin({
            filename: "common.css",
            chunkFilename: "[name].css"
        })
    ],

//----  Core Configuration  ------------------------------------------------------------------------------------------//

    module: {
        rules: [
            {
                test: /\.jsx$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',                    //Using @babel/plugin-proposal-class-properties as
                            '@babel/preset-react'                   //plugin in .babelrc config file is necessary
                        ]
                    }
                }, exclude: /node_modules/
            }, {
                test: /\.scss$/,
                use:
                    [
                        {
                            loader: MiniCssExtractPlugin.loader,    //Avoid using extract-text-webpack-plugin in
                            options: {publicPath: './'}             //webpack 4 or higher. It's no longer supported
                        },
                        {
                            loader: "css-loader",                   // url: false for scss to correctly load bg-image
                            options: {url: false}
                        },                                          //Considering that loaders working right=>left
                        {loader: "postcss-loader"},                 //it is important for postcss-loader to start after
                        {loader: "sass-loader"}                     //sass-loader in order to provide non-SASS syntax.
                    ]                                               //postcss-loader config: postcss.config.js (root)
            }, {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,            //Why use url-loader if using CopyWebpackPlugin?
                loader: 'url-loader?limit=100000'                   //It is necessary for SASS-bootstrap imports
            }]
    },

//----  Output Mode Configuration  -----------------------------------------------------------------------------------//

    mode: 'development',
    optimization: {splitChunks: {chunks: 'all'}},
    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        ignored: /node_modules/,
        poll: 2500
    },

//----  Server Configuration  ----------------------------------------------------------------------------------------//

    devServer: {
        contentBase: path.resolve(__dirname, 'build/assets'),
        stats: 'errors-only',
        open: true,
        port: 3000,
        compress: true,
        historyApiFallback: true
    }
};

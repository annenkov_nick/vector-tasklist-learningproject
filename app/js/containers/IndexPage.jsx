//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react'
//----  Router Imports  ----------------------------------------------------------------------------------------------//
import { Link } from 'react-router-dom'
import { Grid, Row, Col } from 'react-bootstrap'
//----  Components Imports  ------------------------------------------------------------------------------------------//
//
class IndexPage extends React.Component {
    infoContent = [
        {
            title: 'Short overview',
            content: [
                {
                    text: 'This app uses such technologies and practices as WebPack, React, Redux, Router, SCSS, ' +
                        'BEM methodology, React Bootstrap.',
                    type: 'text'
                }, {
                    text: 'Some features described in first task pane (scroll down for link).',
                    type: 'text'
                }, {
                    text: 'Default mode - development.',
                    type: 'text'
                }, {
                    text: 'To build app in development mode w/o starting execute in terminal : ',
                    type: 'text'
                }, {
                    text: '$ npm build',
                    type: 'code'
                }, {
                    text: 'To build app in production mode execute in terminal : ',
                    type: 'text'
                }, {
                    text: '$ npm build-prod',
                    type: 'code'
                }
            ]
        }, {
            title: 'Future goals : ',
            content: [
                {
                    text: <li>To use any API for weather widget with data verification</li>,
                    type: 'text'
                }, {
                    text: <li>Implement error stack using try...catch</li>,
                    type: 'text'
                }, {
                    text: <li>Wrap app in docker</li>,
                    type: 'text'
                }
            ]
        }
    ];

    techLogos = [
        {
            name: 'react-logo.svg',
            width: '212px',
            height: '150px'
        },
        {
            name: 'redux-logo.svg',
            width: '110px',
            height: '110px'
        },
        {
            name: 'react-router-logo.svg',
            width: '120px',
            height: 'none'
        },
        {
            name: 'react-bootstrap-logo.png',
            width: 'none',
            height: '100px'
        },
        {
            name: 'sass-logo.svg',
            width: '110px',
            height: 'none'
        },
        {
            name: 'webpack.svg',
            width: 'none',
            height: '115px'
        }
    ];


    displayInfoContent = (item) => {
        return item.content.map((contentItem, contentIndex) => {
                switch (contentItem.type) {
                    case 'text':
                        return (<div className='info__container__pane__text' key={contentIndex}> {contentItem.text} </div>);
                    case 'code':
                        return (<div className='info__container__pane__code' key={contentIndex}> {contentItem.text} </div>);
                    default:
                        return null
                }
            }
        )
    };

    displayInfo = () => (
        this.infoContent.map((item, index) => {
            return (
                <Row className='info__container__pane' key={index}>
                    <div className='info__container__pane__title'> {item.title} </div>
                    {this.displayInfoContent(item)}
                </Row>
            )
        })
    );

    displayLogos = () => {
        return this.techLogos.map((item, index) => {
            return (
                <Col className='info__container__pane__additional__container__item'
                     xs={4} sm={4} md={6} lg={6}
                     key={index}
                >
                    <img src={`/image/${item.name}`}
                         className='info__container__pane__additional__container__item__icon'
                         style={{'maxHeight': item.height, 'maxWidth': item.width}}
                    />
                </Col>
            )
        });
    };

//----  Render  ------------------------------------------------------------------------------------------------------//
    render() {
        return (
            <div className="homepage-container">
                {/*  ---  Main Content Pane  ---  */}
                <div className='info'>
                    <Grid className='info__container'>
                        <Col xs={12} sm={12} md={8} lg={8}>
                            <Row className='info__container__pane'>
                                <div className='info__container__pane__common-title'>
                                    Learning app additional info
                                </div>
                            </Row>
                            {this.displayInfo()}
                        </Col>
                        <Col className='info__container__pane__additional' xs={12} sm={12} md={4} lg={4}>
                            <div className='info__container__pane__additional__container'>
                                {this.displayLogos()}
                            </div>
                        </Col>
                    </Grid>
                </div>
                <div className="app-link">
                    <div className="app-link__pane">
                        {/*  ---  Title  ---  */}
                        <div className="app-link__pane__title"> Welcome to Vector</div>
                        {/*  ---  Link To App  ---  */}
                        <Link className='app-link__pane__link' to='/app'>
                            <div className='app-link__pane__link__hover'>
                            </div>
                            {/*  ---  Text  ---  */}
                            <div className='app-link__pane__link__container'>
                                <div className='app-link__pane__link__container__text'> Get Started</div>
                                {/*  ---  Add Button  ---  */}
                                <div className='app-link__pane__link__container__button'>
                                    +
                                </div>
                            </div>
                        </Link>
                    </div>
                </div>
                {/*  ---  Background Image Bottom  ---  */}
                {/*<div className="homepage-container__block">*/}
                {/*<img src={"./image/homepage_1.jpg"}*/}
                {/*style={{width: "100%", transform: "rotate(180deg)"}}/>*/}
                {/*</div>*/}
                {/*/!*  ---  Footer  ---  *!/*/}
                {/*<div className="homepage-container__block__footer">*/}
                {/*<div className="footer-container">*/}
                {/*<div>*/}
                {/*{"SOCIAL MEDIA LINKS"}*/}
                {/*</div>*/}
                {/*<div>*/}
                {/*{"Powered by Optimum-WEB"}*/}
                {/*</div>*/}
                {/*</div>*/}
                {/*</div>*/}
            </div>
        )
    }
}

export default IndexPage;
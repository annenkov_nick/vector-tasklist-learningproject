//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react'
//----  Components Imports  ------------------------------------------------------------------------------------------//
import NavMenu from '../components/NavMenu.jsx'
//----  React Bootstrap  ---------------------------------------------------------------------------------------------//
import { Grid, Row, Col } from 'react-bootstrap'

//----  Component Itself  --------------------------------------------------------------------------------------------//
export default class App extends React.Component {
//----  Render  ------------------------------------------------------------------------------------------------------//
    render() {
        return (
            <Grid fluid className='main-grid'>
                {/*  ---  Left Menu  ---  */}
                <Col className='main-grid__navmenu'>
                    <NavMenu/>
                </Col>
                <Col className='main-grid__container'>
                    {/*  ---  Top Image  ---  */}
                    <Row className='main-grid__container__main-image'>
                        <Col className="main-grid__container__main-image__container">
                            <img className="main-grid__container__main-image__container__image"
                                 src={"/image/img1.jpg"}/>
                        </Col>
                    </Row>
                    {/*  ---  Page Content  ---  */}
                    <Row className='main-grid__container__content'>
                        {/*  ---  Task List Container  ---  */}
                        {/*<Row className='todo-list-container'>*/}
                        <Row className='main-grid__container__content__app-content'>
                            {this.props.children}
                        </Row>
                        {/*</Row>*/}
                    </Row>
                </Col>
            </Grid>
        )
    };
}
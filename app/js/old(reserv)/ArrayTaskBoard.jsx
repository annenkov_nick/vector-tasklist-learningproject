//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react'
// import PropTypes from 'prop-types'
//----  React Bootstrap  ---------------------------------------------------------------------------------------------//
import { Row, Grid, Col } from 'react-bootstrap'
import { FormGroup, ControlLabel, FormControl, HelpBlock } from "react-bootstrap"
import FormControlFeedback from "react-bootstrap/lib/FormControlFeedback"

export default class OverView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            validationErrMsg: '' // Message shown when input validation failed
        }
    };


    validation = (e) => {
        let inputValue = e.target.todoInput.value;  // Using FormControl name field to access value
        if (inputValue.length !== 0 && inputValue.length <= 50) {
            this.props.addTodo({'todo': inputValue});   // Avoiding setting data to this.store to use that one in multiple components
            this.setState({validationErrMsg: ''}); // Resetting Validation error message after correct input\
            e.target.todoInput.value = ''; // Resetting input after confirmation
        } else {
            this.setState({validationErrMsg: 'A task should contain from 1 to 50 characters'});
            console.log('most important error message : %cLoh, pidr', 'color:blue')
        }
    };

    inputHandleSubmit = e => {
        e.preventDefault(); // Preventing browser to refresh on Enter button
        this.validation(e);
    };

    placeholderValue = () => {
        let {tasks} = this.props;
        switch (tasks.length) {
            case 0:
                return ('Type your first todo');
            case 5:
                return ("Isn't it enough?");
            case 7:
                return ('Are you insane?');
            default:
                return ('Write another task')
        }
    };

    inputChangeHandler = e => {
    };

    deleteTodoHandle = index => {

        this.props.removeTodo(index);
    };

    render() {
        //  ---  Import Variables  ---  //
        let {tasks} = this.props;
        let {validationErrMsg} = this.state;
        //  ---  Rendering Task List  ---  //
        let displayList = tasks.map((item, index) => {
            return (
                <div className='task-container__li-task__container' key={index}>
                    {/*  ---  Task Element Render ---  */}
                    <p className='task-container__li-task__container__list-item'>{item.todo}</p>
                    {/*  ---  Delete Button ---  */}
                    <div className='task-container__li-task__container__del-button'>
                        <button className='task-container__li-task__container__del-button__item'
                                onClick={() => this.deleteTodoHandle(index)}>
                            <img className="task-container__li-task__container__del-button__item__image"
                                 src={"/image/delete_icon.svg"} alt="Delete Task"/>
                        </button>
                    </div>
                </div>
            )
        });

        return (
            <Grid fluid>
                <div className='separator'>ToDo list made using only arrays</div>
                <div className='main-grid__container__content__app-content'>
                    <div className='component view_max-25_min-20'>
                    <Row className='task-container' xs={6} sm={6} md={4} lg={3}>
                        {/*  ---  Form For Adding Task ---  */}
                        <form className='task-container__form' onSubmit={this.inputHandleSubmit}>
                            <FormGroup controlId='formBasicText' className='task-container__form__form-group'>
                                <ControlLabel/>
                                {/*  ---  Input ---  */}
                                <FormControl className='task-container__form__form-group__input'
                                             type='text'
                                             value={undefined}
                                             placeholder={this.placeholderValue()}
                                             name='todoInput'
                                             onChange={this.inputChangeHandler}
                                />
                                <FormControlFeedback/>
                                <HelpBlock/>
                            </FormGroup>
                            {/*  ---  Add Button  ---  */}
                            <button className='task-container__form__button' type='submit'>+</button>
                        </form>
                        <div className='task-container__validation-err'>{validationErrMsg}</div>
                        {/*  ---  Task List Area  ---  */}
                        <Row className='task-container__li-task'>{displayList}</Row>
                    </Row>
                    </div>
                </div>
            </Grid>
        )
    }
}

// TaskList.propTypes = {
//     tasks: PropTypes.array.isRequired
// };
// -------------------------------------------------------------------------------------------------- //
//    TIPS AND ADDITIONAL COMMENTARY                                                                  //
// -------------------------------------------------------------------------------------------------- //
// export var ping = function ping(store) {                                                           //
//     return function (next) {              ===    // export let ping = store => next => action =>   //
//         return function (action) {        ===    //     console.log('ping')                        //
//             console.log('ping');          ===    //     return next(action)}                       //
//             return next(action);};};};                                                             //
// ---------------------------------------------------------------------------------------------------//

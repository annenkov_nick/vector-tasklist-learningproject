let initialState = {
    tasks: [
        {'todo': 'Action to do 1'},
        {'todo': 'Action to do 2'},
        {'todo': 'Action to do 3'}
    ],
};

import {ADD_TODO, REMOVE_TODO} from '../actions/TaskListActions.jsx'

export let taskListReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO:
            return {
                ...state, tasks: [...state.tasks, action.payload]
            };
        // case REMOVE_TODO:
        //     let taskList = state.tasks;
        //     taskList.splice(action.payload, 1);
        //     return {
        //         ...state, tasks: [...taskList]
        //     };
        default:
            return state;
    }
};
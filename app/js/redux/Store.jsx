//----  Data Imports  ------------------------------------------------------------------------------------------------//
import { createStore, applyMiddleware } from "redux"
import { rootReducer } from "./reducers/Index.jsx"
//----  Middleware Imports  ------------------------------------------------------------------------------------------//
import { logger } from "redux-logger"

export let store = createStore(rootReducer, applyMiddleware(logger));
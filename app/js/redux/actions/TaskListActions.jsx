export const
    ADD_PANE = 'ADD_PANE',
    REMOVE_PANE = 'REMOVE_PANE',
    ADD_TODO = 'ADD_TODO',
    REMOVE_PANE_ITEM = 'REMOVE_PANE_ITEM',
    EDIT_PANE_TITLE = 'EDIT_PANE_TITLE',
    EDIT_PANE_ITEM = 'EDIT_PANE_ITEM',
    TOGGLE_COMPLETION = 'TOGGLE_COMPLETION';

export function addTodoPane(addTodoPaneItem) {
    return {
        type: ADD_PANE,
        payload: addTodoPaneItem
    };
}

export function removeTodoPane(removeTodoPaneItem) {
    return {
        type: REMOVE_PANE,
        payload: removeTodoPaneItem
    };
}

export function addTodo(paneIndex, inputValue){
    return{
        type: ADD_TODO,
        payload: inputValue,
        paneIndex: paneIndex
    }
}

export function removeTodo(todoIndex, paneIndex) {
    return {
        type: REMOVE_PANE_ITEM,
        payload: todoIndex,
        paneIndex: paneIndex
    };
}

export function editPaneTitle(paneIndex, inputValue) {
    return {
        type: EDIT_PANE_TITLE,
        payload: paneIndex,
        value: inputValue
    }
}

export function editPaneItem(todoIndex, paneIndex, inputValue) {
    return {
        type: EDIT_PANE_ITEM,
        payload: todoIndex,
        paneIndex: paneIndex,
        value: inputValue
    };
}

export function toggleTodoCompletionStatus(todoIndex, paneIndex) {
    return {
        type: TOGGLE_COMPLETION,
        payload: todoIndex,
        paneIndex: paneIndex

    }
}


// ---------------------------------------------------------------------------------------- //
//    TIPS AND ADDITIONAL COMMENTARY                                                        //
// ---------------------------------------------------------------------------------------- //
//    Action Core Explanation
//    -----------------------
// Principal action logic:
// Action takes function >argument< from component.jsx by
// this.props.actionFunctionName(>argument<), written in component.jsx,
// and puts this >argument< into payload: >argument<.
// Then reducer gets and uses >argument< by action.payload, written in reducer.
//   ------
// Speaking in detail:
// Considering that action = [{type: 'ACTION_TYPE'}, {payload: >argument<}],
// it becomes clear that reducer checks for 'ACTION_TYPE' in switch (action.type),
// then, on match it uses action.payload (i.e. {payload: >argument<})
// of this action.type (i.e. {type: 'ACTION_TYPE'})
// ---------------------------------------------------------------------------------------- //
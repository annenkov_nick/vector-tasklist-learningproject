let initialState = {
    customMenuItems : {
        0:{
           name: 'home',
           address: 'home'
        },
        1:{
            name: 'Overview',
            address: 'overview'
        },
        2:{
            name: 'New Todo',
            address: 'newtodo'
        },
        3:{
            name: 'New Note',
            address: 'newnote'
        }
    }
};

export let navMenuReducer = (state = initialState) => {
    return (state)
};
let initialState = {
    managerData: [
        {
            title: 'tips to learn .this app features : ',
            toDos: [
                {
                    text: '"Panes" - rendered blocks of task lists and notes',
                    completed: false
                },
                {
                    text: "Click on any pane's element to add a task to already rendered list",
                    completed: false
                },
                {
                    text: 'Try to submit a new or redacted pane data by pressing "enter" key or "+" button, ' +
                        'or by clicking outside the input',
                    completed: false
                },
                {
                    text: 'Try to submit an empty new task. Then try to delete all symbols in rendered task or note and submit',
                    completed: false
                },
                {
                    text: 'Try to add new empty task or note pane',
                    completed: false
                },
                {
                    text: 'Try to delete all panes',
                    completed: false
                }
            ],
            category: 'todo'
        },
        {
            title: 'Some tasks for today',
            toDos: [
                {
                    text: 'Conquer the world',
                    completed: false
                },
                {
                    text: 'Vacuum the room',
                    completed: true
                },
                {
                    text: 'Get full armor set for Warlock on Titan and Io.',
                    completed: false
                },
                {
                    text: 'Open 3rd subclass.',
                    completed: false
                },
                {
                    text: 'Defend the traveler from Cabal.',
                    completed: false
                },
                {
                    text: 'Find Xur and spend some legendary shards',
                    completed: false
                },
                {
                    text: 'Deploy C4 on B at Dust2',
                    completed: false
                },
                {
                    text: 'Do Homework',
                    completed: true
                },
                {
                    text: 'Go to school',
                    completed: true
                }
            ],
            category: 'todo'
        },
        {
            title: 'Some Disturbed lyrics',
            text: 'Like an unsung melody, ' +
                'the truth is waiting there for you to find it. ' +
                'It\'s not a blight, but a remedy - ' +
                'a clear reminder of how it began ' +
                'deep inside your memory, ' +
                'turned away as you struggled to find it. ' +
                'You heard the call as you walked away, ' +
                'a voice of calm from within the silence. ' +
                'And for what seemed an eternity, ' +
                'you\'re waiting, hoping it would call out again. ' +
                'You heard the shadow reckoning, ' +
                'then your fears seemed to keep you blinded. ' +
                'You held your guard as you walked away. ' +
                'When you think all is forsaken ' +
                'listen to me now - ' +
                'you need never feel broken again. ' +
                'Sometimes darkness can show you the light ',
            category: 'note'
        }
    ]
};

import {
    ADD_PANE,
    REMOVE_PANE,
    ADD_TODO,
    REMOVE_PANE_ITEM,
    EDIT_PANE_TITLE,
    EDIT_PANE_ITEM,
    TOGGLE_COMPLETION
} from '../actions/TaskListActions.jsx';

export let taskAndNoteListReducer = (state = initialState, action) => {
    switch (action.type) {
//----  ADD NEW NOTE OR TASK PANE  -----------------------------------------------------------------------------------//
        case ADD_PANE:
            return {
                ...state,
                managerData: [...state.managerData, action.payload]
            };
//----  EDIT SEPARATE TASK OR NOTE  ----------------------------------------------------------------------------------//
        case REMOVE_PANE:
            let panes = [...state.managerData];

            panes.splice(action.payload, 1);

            return {
                ...state,
                managerData: panes
            };
//----  ADD SEPARATE TASK  -------------------------------------------------------------------------------------------//
        case ADD_TODO:
            let copiedStateForAddTodo = JSON.parse(JSON.stringify(state)),
                addTodoData = copiedStateForAddTodo.managerData,
                newTodo = {
                    text: action.payload,
                    completed: false
                };

            addTodoData[action.paneIndex].toDos = [...addTodoData[action.paneIndex].toDos, newTodo];

            return {
                ...state,
                managerData: addTodoData
            };
//----  REMOVE SELECTED NOTE OR SEPARATE TASK  -----------------------------------------------------------------------//
        case REMOVE_PANE_ITEM:
            let clonedState = JSON.parse(JSON.stringify(state)), // Shallow copy have references to the original object,
                allTasks = clonedState.managerData,              // so it doesn't work properly when "children" modify
                paneContent = allTasks[action.paneIndex];        // needed as in this use key

            switch (paneContent.category) {
                case 'todo':
                    paneContent.toDos.splice(action.payload, 1);
                    break;
                case 'note':
                    paneContent.text = action.payload;
                    break;
                default:
                    console.log('TodoManager Reducer: received pane category error')
            }

            return {
                ...state,
                managerData: allTasks
            };
//----  EDIT SELECTED NOTE'S OR SEPARATE TASK'S TITLE  ---------------------------------------------------------------//
        case EDIT_PANE_TITLE:
            let copiedStateForTitleEdit = JSON.parse(JSON.stringify(state)),
                stateData = copiedStateForTitleEdit.managerData;

            stateData[action.payload].title = action.value;

            return {
                ...state,
                managerData: stateData
            };
//----  EDIT SELECTED NOTE OR SEPARATE TASK  -------------------------------------------------------------------------//
        case EDIT_PANE_ITEM:
            let clonedStateForEdit = JSON.parse(JSON.stringify(state)),
                allTasksForEdit = clonedStateForEdit.managerData,
                paneCategory = allTasksForEdit[action.paneIndex].category;


            let valueNotEmpty = (action.value.length > 0),
                setNewValue = (currentTodo) => { currentTodo.text = action.value },
                removeOnEmptyValue = () => { allTasksForEdit[action.paneIndex].toDos.splice(action.payload, 1) },
                makeUncompletedIfEdited = (currentTodo) => {
                    let newValue = action.value,
                        prevValue = currentTodo.text;
                    (newValue !== prevValue && currentTodo.completed) &&
                    (currentTodo.completed = false)
                };
            if (process.env.NODE_ENV !== 'production') {
                console.log()
            }

            switch (paneCategory) {
                case 'todo':
                    let currentTodo = allTasksForEdit[action.paneIndex].toDos[action.payload];
                    valueNotEmpty ?
                        (makeUncompletedIfEdited(currentTodo), setNewValue(currentTodo))
                        : removeOnEmptyValue();
                    break;
                case 'note':
                    let currentNote = allTasksForEdit[action.paneIndex];
                    currentNote.text = action.value;
                    break;
                default:
                    console.log('EDIT_PANE_ITEM error: trying to change text of unrecognized category')
            }

            return {
                ...state,
                managerData: allTasksForEdit
            };
//----  TOGGLE SELECTED TASK COMPLETION STATE  -----------------------------------------------------------------------//
        case TOGGLE_COMPLETION:
            let clonedStateForToggle = JSON.parse(JSON.stringify(state)),
                tasksStack = clonedStateForToggle.managerData,
                completionState = tasksStack[action.paneIndex].toDos[action.payload].completed;

            tasksStack[action.paneIndex].toDos[action.payload].completed = !completionState;

            return {
                ...state,
                managerData: tasksStack
            };
//----  DEFAULT BLOCK  -----------------------------------------------------------------------------------------------//
        default:
            return state;
    }
};
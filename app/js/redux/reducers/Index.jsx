//----  Reducers Import  ---------------------------------------------------------------------------------------------//
import { combineReducers } from 'redux'
import { taskAndNoteListReducer } from './TaskAndNoteList.jsx'
// import { navMenuReducer } from "./NavMenu.jsx"
//----  Reducers  ----------------------------------------------------------------------------------------------------//

export let rootReducer = combineReducers({
    taskAndNoteList : taskAndNoteListReducer,
    // navMenu: navMenuReducer
});
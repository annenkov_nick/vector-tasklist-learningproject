export let errorStack = {
    todoNoteTitleMaxChar: 'Title characters limit reached',
    todoInputMinChar: 'A task should contain at least one character'
};
//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react'
import ReactDOM from 'react-dom'
// import PropTypes from 'prop-types'
//----  Redux-Router Imports  ----------------------------------------------------------------------------------------//
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { addTodoPane } from "../redux/actions/TaskListActions.jsx";
//----  React-Bootstrap Imports  -------------------------------------------------------------------------------------//
import { FormControl, FormGroup } from "react-bootstrap"

class NewNote extends React.Component {
    constructor(props) {
        super(props);
        this.noteTextInputRef = React.createRef();
        this.state = {
            title: '',
            text: '',
            category: 'note'
        }
    }

//----  Title handle submit to this.store  ---------------------------------------------------------------------------//
    inputTitleHandleSubmit = (e) => {
        e.preventDefault();                                         // No need to set input value to empty after submit
        ReactDOM.findDOMNode(this.noteTextInputRef.current).focus();// (text should remain)
    };

    inputTitleOnBlur = (e) => {
        this.setState({title: e.target.value});
    };
//----  Note handle submit to this.state  ----------------------------------------------------------------------------//
    noteValidation = (e) => {
        let noteInput = e.target.value;              // Using FormControl name field to access value
        if (noteInput.length !== 0) {
            this.setState({text: noteInput});
        } else {
            e.target.placeholder = 'A task should contain at least one character';
            console.log('most important error message : %cLoh, pidr', 'color:blue')
        }
    };

    inputNoteOnBlur = (e) => {
        this.noteValidation(e)
    };
//----  Submit new Note to store  ------------------------------------------------------------------------------------//
    addNoteValidation = () => {
        (this.state.text.length !== 0 || this.state.title.length !== 0) &&
        this.props.addTodoPaneAction(this.state)
    };

    addNoteClickHandler = () => {                              // Here happens magic with dispatcher
        this.addNoteValidation()                               // (see bottom of this code page)
    };
//----  Prepare title for render  ------------------------------------------------------------------------------------//
    addTitlePrepared = () => {
        return (
            <form className='task-container__title-form'
                  onSubmit={this.inputTitleHandleSubmit}
                  onBlur={this.inputTitleOnBlur}
                  autoComplete="off"
            >
                <FormGroup className='task-container__title-form__form-group' controlId='formBasicText'>
                    {/*  ---  Input Title ---  */}
                    <FormControl className='task-container__title-form__form-group__title-input'
                                 type='text'
                                 value={undefined}
                                 placeholder='Type title'
                                 name='titleInput'
                                 maxLength='100'
                                 autoFocus
                    />
                </FormGroup>
            </form>
        )
    };
//----  Prepare note for render  -------------------------------------------------------------------------------------//
    addNotePrepared = () => {
        return (
            <form className='task-container__note-form'
                  onBlur={this.inputNoteOnBlur}
                  autoComplete="off"
            >
                <FormGroup className='task-container__note-form__form-group'
                           controlId='formBasicText'
                >
                    {/*  ---  Input Note ---  */}
                    <FormControl className='task-container__note-form__form-group__note-input'
                                 componentClass="textarea"
                                 type='text'
                                 value={undefined}
                                 placeholder='Write down your ideas'
                                 name='noteInput'
                                 ref={this.noteTextInputRef}
                    />
                </FormGroup>
            </form>
        )
    };
//----  Prepare add pane button  -------------------------------------------------------------------------------------//
    addPanePrepared = () => {
        return (
            <div className='task-container__bottom-panel'>
                <Link to={'/app'} className='task-container__bottom-panel__link'>
                    <button className='task-container__bottom-panel__link__add-return-button'
                            onClick={this.addNoteClickHandler}
                    >
                        ✓
                    </button>
                </Link>
            </div>
        )
    };

//----  Render itself  -----------------------------------------------------------------------------------------------//
    render() {
        return (
            <div className='editor-container'>
                <div className='component manager-entity-editor'>
                    <div className='task-container'>
                        {/*  ---  Form For Adding Task Title ---  */}
                        {this.addTitlePrepared()}
                        {/*  ---  Form For Adding Note ---  */}
                        {this.addNotePrepared()}
                        {/*  ---  Pane Submit Button ---  */}
                        {this.addPanePrepared()}

                    </div>
                </div>
            </div>
        )
    }
}


//----  Connection to Store  -----------------------------------------------------------------------------------------//
let mapStateToProps = store => {
    return {}
};

let mapDispatchToProps = dispatch => {
    return {
        addTodoPaneAction: pane => dispatch(addTodoPane(pane))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewNote);
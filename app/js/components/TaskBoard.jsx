//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react'
//----  React-Bootstrap Imports  -------------------------------------------------------------------------------------//
import { Col, FormControl, FormGroup, Grid } from "react-bootstrap";
//----  Component Imports  -------------------------------------------------------------------------------------------//
import { todoCompletionState, displayParams, SCOPE_UNFINISHED, SCOPE_FINISHED } from './TaskListAdditionalData.jsx'
import { errorStack } from '../ErrorStack.jsx'
import PropTypes from 'prop-types'

//----  Modifying data in store  -------------------------------------------------------------------------------------//
class TaskBoard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            taskAdder: []
        }
    }

    componentDidMount() {
        document.addEventListener('click', this.taskPaneFocusListener);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.taskPaneFocusListener);
    }

//--------------------------------------------------------------------------------------------------------------------//
//----  REDUX ACTIONS  -----------------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
    // -- Deleting whole pane
    deletePaneHandler = (index) => {
        this.props.removeTodoPane(index);
    };
    // -- Add separate task
    addTodoItem = (todoInputTarget, index) => {
        todoInputTarget.value.length > 0 ? (
            this.props.addTodo(index, todoInputTarget.value),
                todoInputTarget.value = ''
        ) : todoInputTarget.placeholder = errorStack.todoInputMinChar
    };
    // -- Deleting separate task or clearing note
    deleteTodoHandler = (todoIndex, paneIndex) => {
        this.props.removeTodo(todoIndex, paneIndex)
    };
    // -- Edit selected pane title
    editTitle = (paneIndex, taskList, inputTitleValue) => {
        let prevTitleValue = taskList[paneIndex].title,
            newTitleValue = inputTitleValue.target.textContent;
        prevTitleValue !== newTitleValue &&                          // No need to involve dispatcher if nothing changed
        this.props.editPaneTitle(paneIndex, newTitleValue)
    };
    // -- Edit separate task
    editTask = (todoIndex, paneIndex, taskList, todoInputValue) => {
        let prevTodoValue = taskList[paneIndex].toDos[todoIndex].text,
            newTodoValue = todoInputValue.target.textContent;
        prevTodoValue !== newTodoValue &&                            // No need to involve dispatcher if nothing changed
        this.props.editPaneItem(todoIndex, paneIndex, newTodoValue)
    };
    // -- Edit separate note
    editNote = (paneIndex, taskList, noteInputValue) => {
        let prevNoteValue = taskList[paneIndex].text,
            newNoteValue = noteInputValue.target.textContent;
        prevNoteValue !== newNoteValue &&                            // No need to involve dispatcher if nothing changed
        this.props.editPaneItem(null, paneIndex, newNoteValue)
    };
    // -- Toggle Completion Status of Tasks
    completionStatusToggler = (todoIndex, index) => {
        this.props.toggleTodoCompletionStatus(todoIndex, index)
    };
//--------------------------------------------------------------------------------------------------------------------//
//----  PREPARATIONS FOR RENDER  -------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------------------------------------//
//----  PREPARING TITLE  ---------------------------------------------------------------------------------------------//
    // -- Title color depending on Pane category
    titleStyle = (item) => {
        switch (item.category) {
            case 'todo':
                return ('todo-title');
            case 'note':
                return ('note-title');
            default:
                return ('common-title')
        }
    };
    // -- Preparing pane title
    preparedTitle = (item, index, taskList) => (
        <div className={`task-container__title-container ${this.titleStyle(item)}`}>
            <div className='task-container__title-container__title'
                 contentEditable={true}
                 suppressContentEditableWarning={true}
                 onBlur={this.editTitle.bind(this, index, taskList)}>
                {item.title}
            </div>
            <div className='task-container__title-container__del-button-container'>
                {/*  ---  PANE DELETE BUTTON ---  */}
                <button className='task-container__title-container__del-button-container__button'
                        onClick={() => this.deletePaneHandler(index)}>
                    ×
                </button>
            </div>
        </div>
    );
//----  PREPARING COMPLETED AND UNCOMPLETED TASKS  -------------------------------------------------------------------//
    // -- Focus new task input on Enter button press
    submitEdit = (event) => {
        let renderedTasks = document.getElementsByClassName('task-container__li-task__container__list-item'),
            renderedNewTaskInput = document.getElementsByClassName('task-container__todo-note-form__form-group__todo-note-input');

        renderedTasks = Array.from(renderedTasks);
        renderedNewTaskInput = Array.from(renderedNewTaskInput);

        renderedTasks.map((item) => {                              // Choosing current rendered task
            item.contains(event.target) &&
            item.addEventListener('keypress', (e) => {
                let key = e.which || e.keyCode;

                key === 13 &&                                      // Checking if user pressed Enter
                renderedNewTaskInput.map((item) => {item.focus()}) // Mapping cause HTML shows three items (CSS nuances)
            })
        });
    };

    // -- Preparing tasks for render
    preparedTasks = (item, index, taskList, completionStatus) => (
        taskList[index].toDos.map((todoItem, todoIndex) => // Render only if task completion appropriate to given from parameters
            completionStatus === taskList[index].toDos[todoIndex].completed && (
                <div className={`task-container__li-task__container${displayParams.animation}`} key={todoIndex}
                     onInput={this.submitEdit}>
                    {/*  ---  CHECKBOX ---  */}
                    <div className='task-container__li-task__container__checkbox'
                         onClick={() => this.completionStatusToggler(todoIndex, index)}>
                        {displayParams.icon}
                    </div>
                    {/*  ---  RENDERED TASK TEXT ---  */}
                    <div className={`task-container__li-task__container__list-item${displayParams.styleModifier}`}
                         contentEditable={true}
                         suppressContentEditableWarning={true}
                         onBlur={this.editTask.bind(this, todoIndex, index, taskList)}>
                        {todoItem.text}
                    </div>
                    {/*  ---  DELETE TASK BUTTON ---  */}
                    <div className='task-container__li-task__container__del-button'>
                        <button className='task-container__li-task__container__del-button__item'
                                onClick={() => this.deleteTodoHandler(todoIndex, index)}>
                            {'×'}
                        </button>
                    </div>
                </div>
            )
        )
    );
//----  PREPARING NOTES  ---------------------------------------------------------------------------------------------//
    // -- Note text clear button --
    clearNoteButton = (item, index) => (
        item.text.length > 0 && // render only on text presence
        <div className='task-container__note-container__clear-button'>
            <button className='task-container__note-container__clear-button__item'
                    onClick={() => this.deleteTodoHandler('', index)}>
                {'Clear'}
            </button>
        </div>
    );
    // -- Preparing notes for render
    preparedNotes = (item, index, taskList) => (
        <div className='task-container__note-container' key={index}>
            <div className='task-container__note-container__text'
                 contentEditable={true}
                 suppressContentEditableWarning={true}
                 onBlur={this.editNote.bind(this, index, taskList)}>
                {item.text}
            </div>
            {this.clearNoteButton(item, index)}
        </div>
    );
//----  PREPARING NOTES OR UNCOMPLETED TASKS SWITCHER  ---------------------------------------------------------------//
    // -- Preparing toDos or Note text for render
    preparedContent = (item, index, taskList, completionStatus) => {
        switch (item.category) {
            case 'todo':
                return this.preparedTasks(item, index, taskList, completionStatus);
            case 'note':
                return this.preparedNotes(item, index, taskList);
            default:
                console.log('wrong category')
        }
    };
//----  PREPARING NEW TASK SECTION  ----------------------------------------------------------------------------------//
    inputTodoHandleSubmit = (index, e) => {
        e.preventDefault();
        this.addTodoItem(e.target.todoInput, index);
    };
    todoInputOnBlur = (index, e) => {
        this.addTodoItem(e.target, index);
    };
    // -- Arr to render on separate task-pane
    newTaskInputPlaceholder = [];

    newTaskInput = (index) => (
        <form className='task-container__todo-note-form'
              key={index}
              onSubmit={this.inputTodoHandleSubmit.bind(this, index)}
              autoComplete="off"
              onBlur={this.todoInputOnBlur.bind(this, index)}
        >
            <FormGroup className='task-container__todo-note-form__form-group' controlId='formBasicText'>
                {/*  ---  Input Task ---  */}
                <FormControl className='task-container__todo-note-form__form-group__todo-note-input'
                             type='text'
                             value={undefined}
                             placeholder='Write another task here'
                             name='todoInput'
                />
            </FormGroup>{/*  ---  Separate Task Submitting Button ---  */}
            <button className='task-container__todo-note-form__add-todo-note-button' type='submit'> +</button>
        </form>
    );
    // -- Render new task input on focused task pane only during focus
    taskPaneFocusListener = (event) => {
        let taskContainers = document.getElementsByClassName('task-container'),
            reRender = () => this.setState({reRender: 're-rendered'});

        taskContainers = Array.from(taskContainers);

        return taskContainers.map((item, index) => {
            item.contains(event.target) ? (
                this.newTaskInputPlaceholder[index] = this.newTaskInput(index),
                    reRender()
            ) : (
                this.newTaskInputPlaceholder[index] = null,
                    reRender()
            )
        })
    };
//----  PREPARING COMPLETED TASKS  -----------------------------------------------------------------------------------//
    preparedCompletedTasks = (item, index, taskList) => ( // Checking if category = 'note' to render "completed" section
        taskList[index].category === 'todo' && (
            <>
                {this.newTaskInputPlaceholder[index]}
                <div className='task-container__separator'/>
                <div className='task-container__li-task'>
                    {this.preparedContent(item, index, taskList, true, todoCompletionState(SCOPE_FINISHED))}
                </div>
            </>
        )
    );
//----  PREPARING ALL IN ONE  ----------------------------------------------------------------------------------------//
    displayTaskToDos = (taskList) => (
        taskList.map((item, index) =>
            <Col className='component manager-entity-list' key={index}
                 xs={12} sm={6} md={4} lg={3}
            >
                <div className='task-container'>
                    {/*  ---  PANE TITLE ---  */}
                    {this.preparedTitle(item, index, taskList)}
                    {/*  ---  NOTES OR UNCOMPLETED TASKS ---  */}
                    <div className='task-container__li-task'>
                        {this.preparedContent(item, index, taskList, false, todoCompletionState(SCOPE_UNFINISHED))}
                    </div>
                    {/*  ---  COMPLETED TASKS AND NEW TASK INPUT ---  */}
                    {this.preparedCompletedTasks(item, index, taskList)}
                </div>
            </Col>
        )
    );

//----  FINAL RENDER  ------------------------------------------------------------------------------------------------//
    render() {
        let {taskAndNoteList} = this.props,
            managerData = taskAndNoteList.managerData;

        return (
            <>
                {
                    managerData.length > 0 ?
                        <Grid fluid className='taskboard-container'> {this.displayTaskToDos(managerData)} </Grid>
                        : <Grid fluid className='taskboard-container' style={{'justifyContent': 'center'}}> No panes added </Grid>
                }
            </>
        )
    };
}

TaskBoard.propTypes = {
    taskAndNoteList: PropTypes.object
};

export default TaskBoard;
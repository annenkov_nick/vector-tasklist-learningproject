//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react'
import ReactDOM from 'react-dom'
// import PropTypes from 'prop-types'
//----  Redux-Router Imports  ----------------------------------------------------------------------------------------//
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { addTodoPane } from "../redux/actions/TaskListActions.jsx"
//----  React-Bootstrap Imports  -------------------------------------------------------------------------------------//
import { FormControl, FormGroup } from "react-bootstrap"
//----  Component Imports  -------------------------------------------------------------------------------------------//
import { errorStack } from '../ErrorStack.jsx'
import { todoCompletionState, displayParams, SCOPE_UNFINISHED, SCOPE_FINISHED } from './TaskListAdditionalData.jsx'

class AddTaskPane extends React.Component {

    constructor(props) {
        super(props);
        this.todoInputRef = React.createRef();
        this.state = {                                         // It's better(?) to send a pack of data from state
            title: '',                                         // to store, and then render a package instead of
            toDos: [],                                         // transferring data directly to store, and re-rendering
            category: 'todo'                                   // new pane (not editor) on every submit
        }
    }

//----  Title handle submit to this.store  ---------------------------------------------------------------------------//
    titleChangeHandler = (e) => {
        e.target.value = e.target.value.slice(0, 100);
    };

    inputTitleHandleSubmit = (e) => {
        e.preventDefault();
        ReactDOM.findDOMNode(this.todoInputRef.current).focus();
    };

    titleOnBlur = (e) => {                                     // No need to set input value to empty after submit
        this.setState({title: e.target.value});                // (text should remain)
    };

//----  Separate toDos handle submit to this.state  ------------------------------------------------------------------//
    placeholderValue = () => {
        let {toDos} = this.state;
        switch (toDos.length) {
            case 0:
                return ('Type your first todo here');
            case 5:
                return ("Isn't it enough?");
            case 7:
                return ('Are you insane?');
            default:
                return ('Write another task here')
        }
    };

    todoItemValidation = (todoInputTarget) => {
        todoInputTarget.value.length > 0 ? (
            this.setState({                                    // Used spread operator to imitate function in reducer
                ...this.state,                                 // Let's pretend it's some kind of style
                toDos: [...this.state.toDos, {text: todoInputTarget.value, completed: false}]
            }),
                todoInputTarget.value = ''
        ) : todoInputTarget.placeholder = errorStack.todoInputMinChar
    };

    inputTodoHandleSubmit = (e) => {
        e.preventDefault();
        this.todoItemValidation(e.target.todoInput);
    };

    todoInputOnBlur = (e) => {
        this.todoItemValidation(e.target);
    };
//----  Separate toDos delete handle from this.state  ----------------------------------------------------------------//
    deleteTodoHandler = (index, taskList) => {
        taskList.splice(index, 1);
        this.setState({...this.state, toDos: [...taskList]});
    };
//----  Separate toDos editor  ---------------------------------------------------------------------------------------//
    todoEditOnChangeHandler = (index, renderedTasks, e) => {
        // Can avoid use of brackets in condition, but it makes harder to read the code and understand the logic
        (e.target.textContent !== renderedTasks[index].text && renderedTasks[index].completed === true) && (
            renderedTasks[index].completed = false,
                this.setState({...this.state, toDos: [...renderedTasks]})
        );
    };

    todoEditOnBlurHandler = (index, renderedTasks, e) => {
        e.target.textContent.length > 0 ? (
            this.todoEditOnChangeHandler(index, renderedTasks, e),
                renderedTasks[index].text = e.target.textContent
        ) : (
            renderedTasks.splice(index, 1),
                this.setState({...this.state, toDos: [...renderedTasks]})
        )
    };

    todoCompletionHandler = (index, renderedTasks) => {
        this.setState({...renderedTasks[index].completed = !renderedTasks[index].completed})
    };
//----  Rendering current ToDos   ------------------------------------------------------------------------------------//
    displayToDos = (completionStatus) => {

        let renderedTasks = this.state.toDos;

        return (
            this.state.toDos.map((item, index) => {
                    return (completionStatus === renderedTasks[index].completed &&
                        <div className={`task-container__li-task__container${displayParams.animation}`}
                             key={index + index}>
                            <div className='task-container__li-task__container__checkbox'
                                 onClick={() => this.todoCompletionHandler(index, renderedTasks)}
                            >
                                {displayParams.icon}
                            </div>
                            <div
                                className={`task-container__li-task__container__list-item${displayParams.styleModifier}`}
                                contentEditable={true}
                                suppressContentEditableWarning={true}
                                onBlur={this.todoEditOnBlurHandler.bind(this, index, renderedTasks)}
                            >
                                {item.text}
                            </div>
                            {/*  ---  Delete Button ---  */}
                            <div className='task-container__li-task__container__del-button'>
                                <button className='task-container__li-task__container__del-button__item'
                                        onClick={() => this.deleteTodoHandler(index, renderedTasks)}
                                >
                                    ×
                                </button>
                            </div>
                        </div>
                    )
                }
            )
        )
    }
    ;
//----  Submit new toDoList to store  --------------------------------------------------------------------------------//
    addListValidation = () => {
        (this.state.toDos.length !== 0 || this.state.title.length !== 0) &&
        this.props.addTodoPaneAction(this.state);
    };

    addListClickHandler = () => {                              // Here happens magic with dispatcher
        this.addListValidation()                               // (see bottom of this code page)
    };
//----  Prepare title input  -----------------------------------------------------------------------------------------//
    addTitlePrepared = () => {
        return (
            <form className='task-container__title-form'
                  onSubmit={this.inputTitleHandleSubmit}
                  onBlur={this.titleOnBlur}
                  onChange={this.titleChangeHandler}
                  autoComplete="off"
            >
                <FormGroup className='task-container__title-form__form-group' controlId='formBasicText'>
                    {/*  ---  Input Title ---  */}
                    <FormControl className='task-container__title-form__form-group__title-input'
                                 type='text'
                                 value={undefined}
                                 placeholder='Type title'
                                 name='titleInput'
                                 maxLength='100'
                                 autoFocus
                    />
                </FormGroup>
            </form>
        )
    };
//----  Prepare task input  ------------------------------------------------------------------------------------------//
    addTaskPrepared = () => {
        return (
            <form className='task-container__todo-note-form'
                  onSubmit={this.inputTodoHandleSubmit}
                  autoComplete="off"
                  onBlur={this.todoInputOnBlur}
            >
                <FormGroup className='task-container__todo-note-form__form-group'
                           controlId='formBasicText'
                >
                    {/*  ---  Input Task ---  */}
                    <FormControl className='task-container__todo-note-form__form-group__todo-note-input'
                                 type='text'
                                 value={undefined}
                                 placeholder={this.placeholderValue()}
                                 name='todoInput'
                                 ref={this.todoInputRef}
                    />
                </FormGroup>
                {/*  ---  Separate Task Submitting Button ---  */}
                <button className='task-container__todo-note-form__add-todo-note-button' type='submit'>
                    +
                </button>
            </form>
        )
    };
//----  Prepare pane add button  -------------------------------------------------------------------------------------//
    submitPanePrepared = () => {
        return (
            <div className='task-container__bottom-panel'>
                <Link to={'/app'} className='task-container__bottom-panel__link'>
                    <button className='task-container__bottom-panel__link__add-return-button'
                            onClick={this.addListClickHandler}
                    >
                        ✓
                    </button>
                </Link>
            </div>
        )
    };

//----  Render itself  -----------------------------------------------------------------------------------------------//
    render() {
        return (
            <div className='editor-container'>
                <div className='component manager-entity-editor'>
                    <div className='task-container'>
                        {/*  ---  Form For Adding Task Title ---  */}
                        {this.addTitlePrepared()}
                        {/*  ---  Task List From this.store ---  */}
                        <div className='task-container__li-task'>
                            {this.displayToDos(false, todoCompletionState(SCOPE_UNFINISHED))}
                        </div>
                        {/*  ---  Form For Adding Separate Task ---  */}
                        {this.addTaskPrepared()}
                        <div className='task-container__separator'/>
                        <div className='task-container__li-task'>
                            {this.displayToDos(true, todoCompletionState(SCOPE_FINISHED))}
                        </div>
                        {/*  ---  Pane Add Button ---  */}
                        {this.submitPanePrepared()}
                    </div>
                </div>
            </div>
        )
    }
}

//----  Connection to Store  -----------------------------------------------------------------------------------------//
let mapStateToProps = store => {
    return {};
};

let mapDispatchToProps = dispatch => {
    return {
        addTodoPaneAction: pane => dispatch(addTodoPane(pane))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTaskPane);
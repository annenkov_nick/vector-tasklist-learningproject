export const
    SCOPE_FINISHED = 'SCOPE_FINISHED',
    SCOPE_UNFINISHED = 'SCOPE_UNFINISHED';

export let displayParams;

export function todoCompletionState(scope) {

    switch (scope) {
        case SCOPE_UNFINISHED: {
            displayParams = {
                styleModifier: '',
                animation: ' uncompleted-animation',
                icon: '☐'
            };
            break;
        }
        case SCOPE_FINISHED: {
            displayParams = {
                styleModifier: ' task-completed',
                animation: ' completed-animation',
                icon: '☑'
            };
            break;
        }
        default:
            displayParams = {
                styleModifier: '',
                animation: '',
                icon: ''
            };
    }
}
//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react';
//----  Router Imports  ----------------------------------------------------------------------------------------------//
import { NavLink } from 'react-router-dom'

//----  Render  ------------------------------------------------------------------------------------------------------//
class NavMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            navMenu: 'navMenuIsClosed',
            links: [
                {
                    name: 'Overview',
                    link: '/app'
                },
                {
                    name: 'Todo editor',
                    link: '/app/newtodo'
                },
                {
                    name: 'Note editor',
                    link: '/app/newnote'
                },
                {
                    name: 'Back to Info page',
                    link: '/'
                }
            ]
        }
    }

    switchMenu = () => {
        this.state.navMenu === 'navMenuIsClosed' ?
            this.setState({navMenu: 'navMenuIsOpen'})
            : this.setState({navMenu: 'navMenuIsClosed'});
    };


    shortHandedLink = (item) => {
        return (
            this.state.navMenu === 'navMenuIsClosed' ?
                item.name.split('').splice(0, 1)
                : item.name.toUpperCase()
        )
    };

    hintVisibility = (item, navMenuState) =>{
        return(
            this.state.navMenu === 'navMenuIsClosed' &&
            <div className={`${navMenuState}__container__hint`}>
                <div className={`${navMenuState}__container__hint__text`}>{item.name.toUpperCase()}</div>
            </div>
        )
    };

    renderLinks = (navMenuState) => {
        return (
            this.state.links.map((item, index) => {
                return (
                    <div className={`${navMenuState}__container`} key={index}>
                        <NavLink exact to={item.link} className={`${navMenuState}__container__item`}
                                 activeClassName='nav-link-active'
                        >
                            <button className={`${navMenuState}__container__item__content`}>
                                {this.shortHandedLink(item)}
                            </button>
                        </NavLink>
                        {this.hintVisibility(item, navMenuState)}
                    </div>
                )
            })
        )

    };

    render() {
        let navMenuState = this.state.navMenu;

        return (
            <div className={`${navMenuState}`}>
                {/*  ---  User Avatar  ---  */}
                <button className={`${navMenuState}__menu-state-switcher`} onClick={this.switchMenu}>
                    ☰
                </button>
                <div className={`${navMenuState}__avatar`}>
                    <img className={`${navMenuState}__avatar__user-image`} src={'/image/avatar.jpg'} alt={'Avatar'}/>
                </div>
                {/*  ---  Links  ---  */}
                {this.renderLinks(navMenuState)}
            </div>
        )
    }
}

export default NavMenu;
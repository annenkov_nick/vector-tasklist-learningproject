//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react'
// import PropTypes from 'prop-types'
//----  Redux-Router Imports  ----------------------------------------------------------------------------------------//
import { connect } from 'react-redux'
import {
    removeTodoPane,
    addTodo,
    removeTodo,
    editPaneTitle,
    editPaneItem,
    toggleTodoCompletionStatus
} from "../redux/actions/TaskListActions.jsx"
import { Link } from "react-router-dom"
//----  Components Imports  ------------------------------------------------------------------------------------------//
import TaskBoard from '../components/TaskBoard.jsx'

class OverView extends React.Component {

    appLinks = [
        {
            name: 'taskLink',
            text: 'New Task List',
            color: '#1EB980',
            link: '/app/newtodo'
        }, {
            name: 'noteLink',
            text: 'New Note',
            color: '#ff6859',
            link: '/app/newnote'
        }
    ];

    displayLinks = () => {
        return this.appLinks.map((item, index) => {
            return (
                <Link to={item.link} className='overview__app-links__container' key={index}>
                    <div className='overview__app-links__container__hover'
                         style={{'backgroundColor': `${item.color}`}}/>
                    <button className='overview__app-links__container__item'
                            style={{'borderTop': `1px solid ${item.color}`}}>
                        {item.text}
                    </button>
                </Link>
            )
        });
    };

    render() {
        let {
            taskAndNoteList,
            removeTodoPaneAction,
            addTodo,
            removeTodoAction,
            editPaneTitle,
            editPaneItem,
            toggleTodoCompletionStatus
        } = this.props;
        return (
            <div className='overview'>
                <div className='overview__app-links'>
                    {this.displayLinks()}
                </div>
                <div className='overview__content'>
                    <TaskBoard taskAndNoteList={taskAndNoteList} removeTodoPane={removeTodoPaneAction} addTodo={addTodo}
                               removeTodo={removeTodoAction} editPaneTitle={editPaneTitle} editPaneItem={editPaneItem}
                               toggleTodoCompletionStatus={toggleTodoCompletionStatus}/>
                </div>
            </div>
        )
    };

}

/////  ---- TO MAKE ERROR STACK IN FUTURE ----  /////
//     let errorStack = [];
//     if (!text) {
//     errorStack.push('Not null error');}
// if (text.length === 0) {}

//----  Connection to Store  -----------------------------------------------------------------------------------------//
let mapStateToProps = store => {
    return {
        taskAndNoteList: store.taskAndNoteList
    }
};

let mapDispatchToProps = dispatch => {
    return {
        removeTodoPaneAction: tasks => dispatch(removeTodoPane(tasks)),
        addTodo: (paneIndex, inputValue) => dispatch(addTodo(paneIndex, inputValue)),
        removeTodoAction: (todoIndex, paneIndex) => dispatch(removeTodo(todoIndex, paneIndex)),
        editPaneTitle: (paneIndex, inputValue) => dispatch(editPaneTitle(paneIndex, inputValue)),
        editPaneItem: (todoIndex, paneIndex, inputValue) => dispatch(editPaneItem(todoIndex, paneIndex, inputValue)),
        toggleTodoCompletionStatus: (todoIndex, paneIndex) => dispatch(toggleTodoCompletionStatus(todoIndex, paneIndex))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(OverView);
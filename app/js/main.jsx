//----  React Imports  -----------------------------------------------------------------------------------------------//
import React from 'react'
import ReactDOM from 'react-dom'
//----  Redux Imports  -----------------------------------------------------------------------------------------------//
import { Provider } from 'react-redux'
//----  Router Imports  ----------------------------------------------------------------------------------------------//
import { HashRouter as Router, Route, Switch } from 'react-router-dom'
import { HashRouter } from 'react-router-dom'
//----  Components Imports  ------------------------------------------------------------------------------------------//
import App from './containers/App.jsx'
import OverView from './components/OverView.jsx'
import IndexPage from './containers/IndexPage.jsx'
import AddTaskPane from "./components/AddTaskPane.jsx"
import AddNote  from "./components/AddNote.jsx";
//----  Data Imports  ------------------------------------------------------------------------------------------------//
import { store } from './redux/Store.jsx'
//----  Styles and Style Modules Imports  ----------------------------------------------------------------------------//
//  ---  Bootstrap  ---  //
import 'bootstrap-sass/assets/stylesheets/_bootstrap.scss'        // It's important to follow this order
//  ---  Custom Styles  ---  //                                   // (bootstrap => custom styles)
import '../scss/common.scss'                                      // Otherwise bootstrap will overwrite custom styles
//----  Final Render  ------------------------------------------------------------------------------------------------//
ReactDOM.render(
//     <Provider store={store}>
//         <App/>
//     </Provider>, document.getElementById('root')
// );
    <HashRouter>
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route exact path="/" component={IndexPage}/>
                    <App>
                        <Route exact path="/app" component={OverView}/>
                        <Route exact path="/app/newtodo" component={AddTaskPane}/>
                        <Route exact path="/app/newnote" component={AddNote}/>
                    </App>
                </Switch>
            </Router>
        </Provider>
    </HashRouter>, document.getElementById('root')
);
//----  /Final Render  -----------------------------------------------------------------------------------------------//
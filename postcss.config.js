module.exports = {
    plugins: {
        'cssnano': {},
        'postcss-import': {},
        'postcss-cssnext': {}           //include autoprefixer, no need to use that one separately
    }
};